# Slučaj upotrebe: Igranje igre

### Kratak opis: 
Igrač započinje novu igru unutar glavnog menija aplikacije. Upisuje svoje ime i aplikacija započinje igru. Nakon prelaska igrice, pokazuje se rezultat igrača.

### Akteri:
Igrač

### Preduslovi:
Aplikacija je otvorena i prikazuje glavni meni.

### Postuslovi:
Igrač je uspešno završio igru. Aplikacija prikazuje rezultate i omogućava korisniku dalje akcije (npr. ponovno igranje).

### Osnovni tok događaja:
```
1. Igrač bira dugme "Započni igru" iz glavnog menija.
2. Aplikacija prikazuje igraču tekst za unos imena.
3. Igrač konfiguriše postavke prema svojim željama.
4. Aplikacija inicira novu igru i prikazuje početno stanje igre.
5. Čuva se inicijalno stanje igre i ime igrača.
6. Prelazi se na slučaj upotrebe "Prvi nivo". Po završetku prelazi se na korak 7.
7. Prelazi se na slučaj upotrebe "Drugi nivo". Po završetku prelazi se na korak 8.
8. Prelazi se na slučaj upotrebe "Treći nivo". Po završetku prelazi se na korak 9.
9. Prelazi se na slučaj upotrebe "Četvrti nivo". Po završetku prelazi se na korak 10.
10. Nakon prelaska cele igrice prikazuje se konačni rezultat.
11. Prelazi se na slučaj upotrebe "Čuvanje rezultata".  Po završetku prelazi se na korak 12
12. Aplikacija se vraća u glavni meni.
```

### Alternativni tokovi:
```
A1: Neočekivani izlaz igrača iz aplikacije. Ukoliko u bilo kojem trenutku dođe do prekida rada aplikacije, 
sve do tada sačuvane informacije se trajno čuvaju, a ostalo se poništava. Slučaj upotrebe se završava.
```

### Podtokovi: /
### Specijalni zahtevi: /
### Dodatne informacije: /