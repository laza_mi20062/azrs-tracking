#ifndef NOTE_H
#define NOTE_H
#include <QWidget>
#include <QLabel>
#include <QPushButton>
#include <QVector>
#include <QStringList>
#include <QPaintEvent>
#include "Challenge_test.h"
#include "timer.h"
#include <QMediaPlayer>
#include <QAudioOutput>

class Note : public ChallengeTest
{
    Q_OBJECT
public:
    Note(Timer* timer, int pocetnoVreme, QWidget *parent = nullptr);
    //virtual void zapocniChallenge() override;
    //virtual void zavrsiChallenge() override;
    ~Note();

    QString getIspravnaKombinacija() const { return ispravnaKombinacija; }
    QString getKombinacija() const { return kombinacija; }
    QList<QPushButton*> getNoteIspravnaKombinacija() const { return noteIspravnaKombinacija; }
    int* getBrojac() const { return brojac; }
    void Tekstovi();


public slots:
    //virtual void zapocniChallenge() override;
    void pustiNotu(QString url);

    void kliknuoNotu(QPushButton* clickedButton,QString* kombinacija, QString url);
    void obrni(QPushButton* btnDo,QPushButton* btnRe,QPushButton* btnMi,QPushButton* btnFa,QPushButton* btnSol,QPushButton* btnLa,QPushButton* btnSi,QPushButton* btnDo2);
    void pomoc();





private slots:

    void reprodukcijaZvuka(QMediaPlayer *player);

signals:
         //void pogodjenaRec();

private:
    QWidget *ui;

    QMediaPlayer *nota;
    QAudioOutput *notaAO;

    QPushButton* btnDo;
    QPushButton* btnRe;
    QPushButton* btnMi;
    QPushButton* btnFa;
    QPushButton* btnSol;
    QPushButton* btnLa;
    QPushButton* btnSi;
    QPushButton* btnDo2;

    QString ispravnaKombinacija;
    QString kombinacija;
    QList<QPushButton*> noteKombinacija;
    QList<QPushButton*> noteIspravnaKombinacija;
    int* brojac;



};

#endif // NOTE_H

