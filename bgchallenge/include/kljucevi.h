#ifndef KLJUCEVI_H
#define KLJUCEVI_H
#include "Challenge_test.h"
#include "timer.h"

#include <QMainWindow>
#include <QGraphicsPixmapItem>
#include <QMessageBox>
#include <QGraphicsPixmapItem>
#include <QWidget>
#include <QMediaPlayer>
#include <QAudioOutput>

namespace Ui {
class Kljucevi;
}

class Kljucevi : public ChallengeTest
{
    Q_OBJECT

public:
    explicit Kljucevi(Timer* vreme,int pocetnoVreme, QWidget* parent = nullptr);
    ~Kljucevi();


protected:
    bool eventFilter(QObject *watched, QEvent *event) override;
   // void paintEvent(QPaintEvent *event) override;
    void zvukVrata();

public slots:
    void tekstovi();

private:
    Ui::Kljucevi *m_ui;
    QMediaPlayer *m_player;
    QAudioOutput *m_audioOutput;
    QGraphicsPixmapItem *m_izabranaSlika;
    QPointF m_inicijalnaPozicijaSlike;
    QGraphicsPixmapItem *m_fiksnaSlika1;
    QGraphicsPixmapItem *m_fiksnaSlika2;
    QGraphicsPixmapItem *m_fiksnaSlika3;
    QGraphicsPixmapItem *m_fiksnaSlika4;
    QGraphicsPixmapItem *m_pokretnaSlika1;
    QGraphicsPixmapItem *m_pokretnaSlika2;
    QGraphicsPixmapItem *m_pokretnaSlika3;
    QGraphicsPixmapItem *m_pokretnaSlika4;
    int m_brojKljuceva;
    QTimer m_timer;
    //QString imagePath;


    QGraphicsScene *m_scene;

};

#endif // KLJUCEVI_H

