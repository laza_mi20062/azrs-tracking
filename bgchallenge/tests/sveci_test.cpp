#include <gtest/gtest.h>
#include "../include/sveci.h"
#include "../include/timer.h"
#include "../forms/ui_sveci.h"

#include <QApplication>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QSignalSpy>


TEST(SveciTest, ProveraIspravnostiUnosa) {
    int argc = 0;
    char* argv[] = { nullptr };
    QApplication app(argc, argv);

    Timer* testTimer = new Timer();
    Sveci* sveci = new Sveci(testTimer, 0, nullptr);

    // Testiranje funkcije proveri()
    int inicijalniBrojWidzeta = sveci->ui->horizontalLayout->count();
    sveci->ui->lineEdit->setText("SAVA");
    sveci->proveri();
    EXPECT_GT(sveci->ui->horizontalLayout->count(), inicijalniBrojWidzeta);

    delete sveci;
    delete testTimer;
}

TEST(SveciTest, ProveraNeispravnostiUnosa) {
    int argc = 0;
    char* argv[] = { nullptr };
    QApplication app(argc, argv);

    Timer* testTimer = new Timer();
    Sveci* sveci = new Sveci(testTimer, 0, nullptr);

    // Testiranje funkcije proveri()
    int inicijalniBrojWidzeta = sveci->ui->horizontalLayout->count();
    sveci->ui->lineEdit->setText("Nepostojeci unos");
    sveci->proveri();
    EXPECT_EQ(sveci->ui->horizontalLayout->count(), inicijalniBrojWidzeta);
    EXPECT_EQ(sveci->ui->lineEdit->text(), "");

    delete sveci;
    delete testTimer;
}

int main(int argc, char** argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
